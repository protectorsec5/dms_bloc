// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent = void Function(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  @override
  String get localeName => 'en';

  static String m0(task) => 'Deleted "${task}"';

  @override
  final messages = _notInlinedMessages(_notInlinedMessages);

  static Map<String, dynamic> _notInlinedMessages(_) => {
        'activeTodos': MessageLookupByLibrary.simpleMessage('Doly seredilmedi'),
        'addTodo': MessageLookupByLibrary.simpleMessage('Maglumat goş'),
        'cancel': MessageLookupByLibrary.simpleMessage('Goýbolsun et'),
        'clearCompleted': MessageLookupByLibrary.simpleMessage(
            'Ýerine ýetirleni sanawdan aýýyr'),
        'completedTodos':
            MessageLookupByLibrary.simpleMessage('Ýerine yetirildi'),
        'delete': MessageLookupByLibrary.simpleMessage('Delete'),
        'deleteTodo': MessageLookupByLibrary.simpleMessage('Aýyr'),
        'deleteTodoConfirmation':
            MessageLookupByLibrary.simpleMessage('Aýyrmakçymy?'),
        'editTodo': MessageLookupByLibrary.simpleMessage('Üýtget'),
        'emptyTodoError':
            MessageLookupByLibrary.simpleMessage('Please enter some text'),
        'filterTodos': MessageLookupByLibrary.simpleMessage('Sanawy Filterle'),
        'markAllComplete':
            MessageLookupByLibrary.simpleMessage('Ählisi ýerine ýetirildi'),
        'markAllIncomplete':
            MessageLookupByLibrary.simpleMessage('Ählisi ýerine ýetirildi'),
        'newTodoHint':
            MessageLookupByLibrary.simpleMessage('Maglumatyňyz näme barada?'),
        'notesHint':
            MessageLookupByLibrary.simpleMessage('goşmaça maglumatlar...'),
        'saveChanges':
            MessageLookupByLibrary.simpleMessage('üýtgetmeleri ýatda sakla'),
        'showActive': MessageLookupByLibrary.simpleMessage('Seredilmedik'),
        'showAll': MessageLookupByLibrary.simpleMessage('Ählisi'),
        'showCompleted':
            MessageLookupByLibrary.simpleMessage('Ýerine ýetirlenler'),
        'stats': MessageLookupByLibrary.simpleMessage('Netije'),
        'todoDeleted': m0,
        'todoDetails': MessageLookupByLibrary.simpleMessage('Todo Details'),
        'todos': MessageLookupByLibrary.simpleMessage('Ählisi'),
        'undo': MessageLookupByLibrary.simpleMessage('Undo')
      };
}
